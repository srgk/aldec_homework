#include <stdio.h>
#include <math.h>

int main() {
	double polRadius, polAngle, decX, decY;
	printf("Input polar coordinates of your point: ");
	scanf("%lf%lf", &polAngle, &polRadius);
	decX = polRadius * cos(polAngle);
	decY = polRadius * sin(polAngle);
	printf("Point has coordinates (%lf, %lf) in Cartesian coordinate system", decX, decY);
	return 0;
}
