#include <stdio.h>
#include <math.h>

struct Point
{
	double x;
	double y;
};

Point readPoint()
{
	Point p;
	scanf( "%lf %lf", &( p.x ), &( p.y ) );
	return p;
}

struct Vector {
	Point coordinate;
	double length;
};

void getVector(Vector * vector, Point a, Point b )
{
	vector->coordinate.x = b.x - a.x;
	vector->coordinate.y = b.y - a.y;
	vector->length = sqrt( vector->coordinate.x * vector->coordinate.x + vector->coordinate.y * vector->coordinate.y );
}

struct Sector
{
	Point p2, p3, center;
	Vector side1, side2;
};

void getRadiusVectors(Sector * pSector) {
	getVector(&pSector->side1, pSector->center, pSector->p2);
	getVector(&pSector->side2, pSector->center, pSector->p3);
}

void readSector( Sector * pSector )
{
	pSector->center = readPoint();
	pSector->p2 = readPoint();
	pSector->p3 = readPoint();
}

bool isApproxEqual(const double * value1, const double * value2) {
	double difference = *value1 - *value2;
	return ((-0.001 < difference) && (difference < 0.001));
}

enum errors {
	EQUAL_POINTS = 1, DIFF_RADIUS
};

void printError(errors id) {
	printf("ERROR: ");
	switch (id) {
	case EQUAL_POINTS: printf("Your points of a cicle is equal. "); break;
	case DIFF_RADIUS: printf("One of the ponts doesn't belong to the circle. "); break;
	}
}

bool checkPoints( const Sector * pSector )
{
	if ((pSector->p2.x == pSector->p3.x) && (pSector->p2.y == pSector->p3.y)) {
		printError(EQUAL_POINTS);
		return false;
	}
	if (!(isApproxEqual(&pSector->side1.length, &pSector->side2.length))) {
		printError(DIFF_RADIUS);
		return false;
	}
	return true;
}

double getCosinus(const Point * vector1, const Point * vector2)
{
	return ((vector1->x * vector2->x + vector1->y * vector2->y) /
			sqrt((vector1->x * vector1->x + vector1->y * vector1->y) *
			(vector2->x*vector2->x + vector2->y*vector2->y)));
}

double getSectorArea( Sector * pSector )
{

	double angle = acos(getCosinus(&pSector->side1.coordinate, &pSector->side2.coordinate));
	return(angle / 2 * pSector->side1.length * pSector->side1.length);
}

int main() {
	Sector s;
	printf("Input here coordinates for the center of the circle and for two points that belong this circle (in format x1 y1 x2 y2 x3 y3): ");
	readSector( & s );
	getRadiusVectors( & s );
	while (!checkPoints( & s )) {
		printf( "Try again: " );
		readSector( & s );
	}

	printf("OK. Your figure area is: %lf square units\n", getSectorArea( & s ));

	return 0;
}
