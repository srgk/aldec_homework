#include <stdio.h>
#include <math.h>


struct Point
{
	double x;
	double y;
};


Point readPoint ()
{
	Point p;
	scanf( "%lf %lf", &( p.x ), &( p.y ) );
	return p;
}


double getDistance ( Point a, Point b )
{
	double xDiff = b.x - a.x;
	double yDiff = b.y - a.y;
    return sqrt( xDiff * xDiff + yDiff * yDiff );
}


struct Triangle
{
	Point p1, p2, p3;
};


void readTriangle ( Triangle * pTriangle )
{
	pTriangle->p1 = readPoint();
	pTriangle->p2 = readPoint();
	pTriangle->p3 = readPoint();
}


void calculateTriangleSides ( const Triangle * pTriangle, double * pSide12, double * pSide13, double * pSide23 )
{
    * pSide12 = getDistance( pTriangle->p1, pTriangle->p2 );
    * pSide13 = getDistance( pTriangle->p1, pTriangle->p3 );
    * pSide23 = getDistance( pTriangle->p2, pTriangle->p3 );
}


bool checkTriangleRule ( const Triangle * pTriangle )
{
    double side12, side13, side23;
	calculateTriangleSides( pTriangle, & side12, & side13, & side23 );

    return ( side12 < ( side13 + side23 ) ) &&
           ( side13 < ( side12 + side23 ) ) &&
           ( side23 < ( side12 + side13 ) );
}


double getFigureArea ( const Triangle * pTriangle )
{
    double side12, side13, side23;
	calculateTriangleSides( pTriangle, & side12, & side13, & side23 );

    double halfPerimeter = ( side12 + side13 + side23 ) / 2.0;

    double triangleArea = sqrt(
            halfPerimeter *
            ( halfPerimeter - side12 ) *
            ( halfPerimeter - side13 ) *
            ( halfPerimeter - side23 ) );

    double incircleRadius = (triangleArea / halfPerimeter);

    double incircleArea = (incircleRadius * incircleRadius * M_PI);

    return (triangleArea - incircleArea);
}

int main() {
	Triangle t;
	printf("Input here coordinates of 3 triangle points (in format x1 y1 x2 y2 x3 y3): ");
	readTriangle( & t );

	//Как лучше делать в следующих задачах: давать пользователю второй шанс,если он введет белиберду, или сразу выходить?
	while (!checkTriangleRule( & t )) {
		printf( "Not a triangle, try again: \n" );
	}

	printf("OK. Your figure area is: %lf square units\n", getFigureArea( & t ));

	return 0;
}
