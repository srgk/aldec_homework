#include <stdio.h>

bool isaLetter(int symbol)
{
	return (((symbol >= 'a') && (symbol <= 'z')) || ((symbol >= 'A') && (symbol <= 'Z')));
}

int main()
{
	int injectedCharacter;
	short int numOfVovel = 0, numOfConsonants = 0;

	printf("Input sequence of symbols and press Ctrl+Z (or Ctrl+D\nfor Unix-like OS's) when you will be ready for result:\n");

	while ((injectedCharacter=getchar())!=EOF)
	{
		if (isaLetter(injectedCharacter))
		{
			switch(injectedCharacter)
			{
				case 'a': case 'e': case 'i': case 'o': case 'u':
				case 'A': case 'E': case 'I': case 'O': case 'U':
					numOfVovel++;
					break;

				default:
					numOfConsonants++;
			}
		}
	}

	printf("\nYou entered %d vovels and %d consonants\n", numOfVovel, numOfConsonants);
	return 0;
}
