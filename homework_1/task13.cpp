#include <stdio.h>
#include <stdlib.h>

enum events { ERR_NAN = 1, ERR_NEG, ERR_NAI, WARN_IGNORE, ERR_TOOBIG};

void printEvent(events eventId) {
	switch (eventId) {
	case ERR_NAN: printf("Error: Not a number at all. "); break;
	case ERR_NEG: printf("Error: Negative number. "); break;
	case ERR_NAI: printf("Error: Not an integer or it is too big. "); break;
	case ERR_TOOBIG: printf("Sorry. This small program can't handle such big numbers. At least for now. "); return;
	case WARN_IGNORE: printf("Warning: Using only first found number as an input. ");
	}
	printf("Only one small (< 21) positive integer allowed\n");
}

void bufferNormalizer() {
	while (getchar() != '\n');
}

void getNumber(int& returned) {
	double input;
	while (true) {
		printf("Your number, please: ");
		if (!(scanf("%lf", &input))) {
			bufferNormalizer();
			printEvent(ERR_NAN);
			continue;
		}
		if (getchar() != '\n') {
			printEvent(WARN_IGNORE);
			bufferNormalizer();
		}
		if (input < 0) {
			printEvent(ERR_NEG);
			continue;
		}
		returned = (double)(int)input;
		if (input != returned )
		{
			printEvent(ERR_NAI);
			continue;
		}
		if (input > 20) {
			printEvent(ERR_TOOBIG);
			/*
			 * Нужно ли выводить строчку ниже - не понятно. Если пользователь все-таки введет огромное число
			 * (которое не влезет в double), то в returned может попасть неправильное число и программа даст
			 * юзеру неправильную ссылку (как минимум, не на то целое положительное число, которое он ввел).
			 */
			printf("Maybe, this link will help you: http://www.wolframalpha.com/input/?i=%d!\n", returned);
			exit(EXIT_FAILURE);
		}
		break;
	}
}

unsigned long long getFact(int value) {
	if (!value) {
		return 1;
	}
	unsigned long long result = value;
	for(value--; value != 1; value--) {
		result*=value;
	}
	return result;
}

int main() {
	int input;
	unsigned long long result;
	getNumber(input);
	result = getFact(input);
	printf("%d! = %llu", input, result);
	return 0;
}
