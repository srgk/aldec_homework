#include <stdio.h>
#include <math.h>

double findVectorCoordinate(double vectorStart, double vectorEnd)
{
	return (vectorEnd - vectorStart);
}

double getCosinus(double m1, double n1, double m2, double n2)
{
	return ((m1*m2+n1*n2) /
			sqrt(((m1*m1+n1*n1)*(m2*m2+n2*n2))));
}

void getIntersect(double m1, double n1, double m2, double n2, double x1, double y1, double x2, double y2, double& x, double& y) {
	x=((x1*m2-x2*m1)/(m2-m1));
	y=((y1*n2-y2*n1)/(n2-n1));
}

int main()
{
	double dot1x, dot1y, dot2x, dot2y, dot3x, dot3y, dot4x, dot4y, xVector1, yVector1, xVector2, yVector2, cosinus;
	printf("Input coordinates of 4 points in format:\nx1 y1 x2 y2 x3 y3 x4 y4: ");
	scanf("%lf %lf %lf %lf %lf %lf %lf %lf", &dot1x, &dot1y, &dot2x, &dot2y, &dot3x, &dot3y, &dot4x, &dot4y);
	xVector1 = findVectorCoordinate(dot1x, dot2x);
	yVector1 = findVectorCoordinate(dot1y, dot2y);
	xVector2 = findVectorCoordinate(dot3x, dot4x);
	yVector2 = findVectorCoordinate(dot3y, dot4y);

	cosinus = getCosinus(xVector1, yVector1, xVector2, yVector2);
	if ((cosinus == 1) || (cosinus == -1))
	{
		printf("Straights are parallel");
	}
	else {
		double angle, intersectX, intersectY;
		angle = acos(cosinus);
		getIntersect(xVector1, yVector1, xVector2, yVector2, dot1x, dot1y, dot3x, dot3y, intersectX, intersectY);
		printf("Straights intersect in point (%lf; %lf) at an angle %lf rad\n", intersectX, intersectY, angle);
	}
	return 0;
}
