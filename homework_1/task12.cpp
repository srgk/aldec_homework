#include <stdio.h>
#include <string.h>

struct twoChars {
	char prev, curr;
};

struct commentStatus {
	bool m_justOpened = false, m_justClosed = false;
	int m_counter = 0;
};

struct CharVector {
	int m_nUsed, m_nAllocated;
	char * m_pData;
};

void initVector (CharVector &_v, int _size) {
	_v.m_nAllocated = _size;
	_v.m_nUsed = 0;
	_v.m_pData = new char[_size];
}

//По умолчанию произойдет увеличение вектора в два раза
void growVector (CharVector &_v, int _multiplicator = 2) {
	// Выделяем новый блок, в 2 раза больше прежнего
	int nAllocatedNew = _v.m_nAllocated * _multiplicator;
	char * pNewData = new char[ nAllocatedNew ];

	// Копируем данные из прежнего блока в новый
	memcpy(
		pNewData,
		_v.m_pData,
		sizeof( char ) * _v.m_nAllocated
	);

	// Освобождаем старый блок и подменяем адрес на новый блок
	delete[] _v.m_pData;
	_v.m_pData = pNewData;

	// Обновляем количество выделенных ячеек
	_v.m_nAllocated = nAllocatedNew;

}

void addCharTo (CharVector &_v, int _index, char _c) {
	if (_v.m_nAllocated == _v.m_nUsed ) {
		growVector(_v);
	}
	_v.m_pData[_index] = _c;
	_v.m_nUsed++;
}

void deleteVector (CharVector &_v) {
	delete[] _v.m_pData;
}

void getClearedString (CharVector &_v) {
	//Временная структура для вводимых символов
	twoChars carrier;
	//Служебные данные
	commentStatus status;
	//Перед началом цикла получаем первый символ
	carrier.curr = getchar();
	//Цикл заполнения массива символов
	for ( int i = 0 ;; ) {
		carrier.prev = carrier.curr;
		//Если мы нашли символ перевода строки - значит пользовтель ввел все, что хотел
		if (carrier.prev == '\n') {
			//Предумпредим его, если он забыл закрыть комментарий
			if (status.m_counter > 0) {
				printf("You have some unclosed comments. So everyting from comment opening to the end of the string will be ignored\n");
			}
			//Но в любом случае закроем строку...
			addCharTo (_v, i, '\0');
			//...и выйдем из цикла и, на самом деле, из всей функции.
			break;
		}

		//Но если конца строки еще не видно - продолжаем обработку
		carrier.curr = getchar();

		//Корректно обрабатываем случай, когда сразу после закрытого комментария
		//у нас попадается звездочка. Пример: "OK, /*ingored*/*not ignored".
		if ((carrier.prev == '/') && (status.m_justClosed)) {
			status.m_justClosed = false;
			continue;
		}

		status.m_justClosed = false;

		//Ловим открытие комментария
		if ((carrier.prev == '/') && (carrier.curr == '*')) {
			status.m_counter++;
			status.m_justOpened = true;
			continue;
		}

		//Закрываем комментарии. Отрабатывает только тогда, когда у нас есть открытые комментарии. Что логично.
		if ((carrier.prev == '*') && (carrier.curr == '/') && (status.m_counter > 0)) {
			//У нас есть два случая, когда встречается последовательсность символов '/*/':
			//1) комментарий только-только открылся, тогда выополняем else-блок;
			//2) комментарий уже несколько раз открывался, но никак не закроется.
			//Во втором и во всех остальных случаях закрываем комментарий.

			if (!status.m_justOpened || (status.m_justOpened && (status.m_counter > 1))) {
				status.m_counter = 0;
				status.m_justClosed = true;
				status.m_justOpened = false;
				continue;
			}
		}

		status.m_justOpened = false;

		if (status.m_counter > 0) {
			continue;
		}

		addCharTo (_v, i++, carrier.prev);
	}
}

int main() {
	const int initSize = 30;
	CharVector string;
	initVector(string, initSize);
	printf("Type your source string here: ");
	getClearedString (string);
	printf("Your cleaned string is: %s\n", string.m_pData);
	deleteVector(string);
	return 0;
}
