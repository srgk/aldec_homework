#include <stdio.h>

int main() {
	double lengthInMeters, lengthInInches;
	printf("Input your length value in meters: ");
	scanf("%lf", &lengthInMeters);
	if(lengthInMeters < 0) {
		printf("Only possitive values is acceptable");
		return -1;
	}
	lengthInInches = lengthInMeters*100/2.54;
	printf("%lf meters = %lf inches", lengthInMeters, lengthInInches);
	return 0;
}
