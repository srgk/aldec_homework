#!/bin/bash
shopt -s globstar
SOURCE=$(dirname ${BASH_SOURCE})
echo "is $SOURCE correct source directory?"
select yn in "Yes" "No"; do
   case $yn in
	Yes ) break;;
	No ) echo -n "Input correct source, please: "; read SOURCE ; break;;
   esac
done

if [ ! -d "$SOURCE/deploy" ]; then
   mkdir "$SOURCE/deploy"
fi

echo -e "\n\033[0;31mCopying...\033[0m"
for i in $SOURCE/hw* ;
do
   numOfHw=$( echo $i | sed -e "s|$SOURCE||g" | cut -d _ -f 1 | cut -c 4- )
   mkdir -p "$SOURCE/deploy/homework_${numOfHw}"
   for j in $i/**/*.cpp;
   do
       cp -v "$j" "$SOURCE/deploy/homework_${numOfHw}/"
   done
done

